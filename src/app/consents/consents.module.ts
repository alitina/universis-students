import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { ConsentsRoutingModule } from './consents-routing.module';
import { ConsentsSharedModule } from './consents-shared.module';
import { NgPipesModule } from 'ngx-pipes';
import { ConsentsHomeComponent } from './components/consents-home/consents-home.component';
import { ConsentGroupComponent } from './components/consent-group/consent-group.component';
import { NgxConsentModule } from '@universis/ngx-consents';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MostModule,
    ConsentsRoutingModule,
    ConsentsSharedModule,
    NgPipesModule,
    NgxConsentModule
  ],
  declarations: [
    ConsentsHomeComponent,
    ConsentGroupComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsentsModule { }
