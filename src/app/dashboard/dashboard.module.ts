import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { NgChartsModule } from 'ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MostModule } from "@themost/angular";
import { CommonModule } from "@angular/common";
import { GradesModule } from '../grades/grades.module';
import { ProgressBarDegreeComponent } from './components/progress-bar-degree/progress-bar-degree.component';
import { ProgressBarSemesterComponent } from './components/progress-bar-semester/progress-bar-semester.component';
import { StudentRecentCoursesComponent } from './components/student-recent-courses/student-recent-courses.component';
import { StudentRecentGradesComponent } from './components/student-recent-grades/student-recent-grades.component';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SharedModule } from '@universis/common';
import { EventsModule } from '@universis/ngx-events';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import * as el from "./i18n/dashboard.el.json"
import * as en from "./i18n/dashboard.en.json"
import { DiningModule } from '@universis/ngx-dining';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgChartsModule,
    TranslateModule,
    MostModule,
    GradesModule,
    MostModule,
    TooltipModule,
    BsDropdownModule,
    SharedModule,
    DiningModule,
    EventsModule
  ],
  declarations: [DashboardComponent,
    ProgressBarDegreeComponent,
    ProgressBarSemesterComponent,
    StudentRecentCoursesComponent,
    StudentRecentGradesComponent],
  bootstrap: [DashboardComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DashboardModule {
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }

}
