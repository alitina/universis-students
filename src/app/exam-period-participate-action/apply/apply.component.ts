import { Component, OnInit, TemplateRef, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ModalService, ToastService, DIALOG_BUTTONS, LoadingService, ConfigurationService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { cloneDeep } from 'lodash';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { Converter } from 'showdown';


@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html'
})
export class ApplyComponent implements OnInit {

  public data: any;
  public department: any;
  public student: any;
  public selectionBadge: string | any;
  public loading = true;
  public invalidConfiguration = false;
  public invalidPeriod = false;
  public terms: any;
  constructor(private _context: AngularDataContext,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _modalService: ModalService,
    private _router: Router,
    private _toastService: ToastrService,
    private _loadingService: LoadingService,
    private _profileService: ProfileService,
    private _http: HttpClient,
    private _configurationService: ConfigurationService) { }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  async ngOnInit() {
    try {
      this.showLoading(true);
      // get department data
      this.department = await this._context.model('students/me/department')
        .asQueryable()
        .expand('departmentConfiguration($expand=examYear,examPeriod)')
        .getItem();
      this.student = await await this._profileService.getStudent();
      // validate configuration
      if (this.department && this.department.departmentConfiguration == null) {
        this.invalidConfiguration = true;
      }
      if (this.department && this.department.departmentConfiguration) {
        this.invalidConfiguration = (this.department.departmentConfiguration.examYear == null)
          || (this.department.departmentConfiguration.examPeriod == null);
      }

      // validate that request does not exist
      if (!this.invalidConfiguration) {
        const examPeriodParticipateAction = await this._context.model('ExamPeriodParticipateActions')
          .where('student').equal(this.student.id)
          .and('year').equal(this.department.departmentConfiguration.examYear.id)
          .and('examPeriod').equal(this.department.departmentConfiguration.examPeriod.id)
          .select('id')
          .getItem();
        if (examPeriodParticipateAction != null) {
          return this._router.navigate(['/requests', 'ExamPeriodParticipateActions', examPeriodParticipateAction.id, 'preview']);
        }
      }

      // validate period start and end
      if (this.department && this.department.departmentConfiguration) {
        const periodStart: Date = this.department.departmentConfiguration.examParticipationPeriodStart;
        const periodEnd: Date = this.department.departmentConfiguration.examParticipationPeriodEnd;
        if (periodStart instanceof Date) {
          // format date because of a limitation of translate service
          // that cannot use pipes inside parameters
          this.department.departmentConfiguration.examParticipationPeriodStartDate =
            new DatePipe(this._translateService.currentLang)
              .transform(this.department.departmentConfiguration.examParticipationPeriodStart, 'mediumDate');
          if (periodStart > new Date()) {
            // period has not been started yet
            this.invalidPeriod = true;
          }
        }
        if (periodEnd instanceof Date) {
          // format date because of a limitation of translate service
          // that cannot use pipes inside parameters
          this.department.departmentConfiguration.examParticipationPeriodEndDate =
            new DatePipe(this._translateService.currentLang)
              .transform(this.department.departmentConfiguration.examParticipationPeriodEnd, 'mediumDate');
          periodEnd.setDate(periodEnd.getDate() + 1);
          if (periodEnd < new Date()) {
            // period has ended
            this.invalidPeriod = true;
          }
        }
      }
      if (this.invalidConfiguration || this.invalidPeriod) {
        // exit
        this.data = {};
        this.showLoading(false);
        return;
      }
      if (this.student && this.student.studentStatus && this.student.studentStatus.alternateName !== 'active') {
        // exit
        this.data = {};
        this.showLoading(false);
        return;
      }
      // get course data
      let result = await this._context.model('Students/Me/AvailableExamsForParticipation')
        .asQueryable()
        .getItems();
      result = result || [];
      // map result
      const courseExamActions = result.map(x => {
        return {
          courseExam: x,
          agree: false,
          student: this.student.id
        };
      });
      // get current language
      const currentLocale = this._configurationService.currentLocale;
      // get md file if any;
      const termsFile = `assets/docs/ExamPeriodParticipateAction.terms.${currentLocale}.md`
      this._http.get(termsFile, { responseType: 'text' }).subscribe((terms) => {
        this.terms = new Converter({ tables: true }).makeHtml(terms);
      });
      const student = this.student;
      // set agree to false (leave user to select)
      const agree = false;
      // set reject to false (leave user to select)
      const reject = false;
      // set selected
      const selected = 0;
      // set total
      const total = courseExamActions.length;
      // get academic year
      const year = this.department.departmentConfiguration.examYear;
      // get exam period
      const examPeriod = this.department.departmentConfiguration.examPeriod;
      // set data
      this.data = {
        agree,
        reject,
        courseExamActions,
        selected,
        total,
        year,
        examPeriod
      };
      this.onSelectionChange();
      this.showLoading(false);
    } catch (err) {
      this.showLoading(false);
      this._errorService.showError(err);
    }
  }

  submit(form: NgForm) {
    // getr confirm message for accept or reject
    let ConfirmMessage: string = "";
    const ConfirmMessageTitle = this._translateService.instant('ExamPeriodParticipateActions.Title.One');
    if (this.data.agree) {
      ConfirmMessage = this._translateService.instant('ExamPeriodParticipateActions.ConfirmMessage.Accept', this.data);
    } else if (this.data.reject) {
      ConfirmMessage = this._translateService.instant('ExamPeriodParticipateActions.ConfirmMessage.Reject');
    }
    // show dialog
    this._modalService.showDialog(ConfirmMessageTitle, ConfirmMessage, DIALOG_BUTTONS.OkCancel).then(result => {
      if (result === 'ok') {
        this.showLoading(true);
        const submissionData = cloneDeep(this.data);
        // format payload
        submissionData.agree = submissionData.agree ? 1 : 0;
        submissionData.year = submissionData.year.id;
        submissionData.examPeriod = submissionData.examPeriod.id;
        submissionData.courseExamActions.forEach(element => {
          element.courseExam = element.courseExam.id;
          element.agree = element.agree ? 1 : 0;
        });
        this._context.model('ExamPeriodParticipateActions')
          .save(submissionData).then(() => {
            // get complete message
            const CompleteMessage = this._translateService.instant('ExamPeriodParticipateActions.CompleteMessage');
            // hide loading
            this.showLoading(false);
            // show toast message
            this._toastService.success(CompleteMessage, ConfirmMessageTitle,
              { timeOut: 3000, positionClass: 'toast-top-center', progressBar: false });
            // navigation to messages
            this._router.navigate(['/messages']);
          }).catch(err => {
            this.showLoading(false);
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
      }
    });
  }

  onAgreeChange() {
    if (this.data.agree === false) {
      this.data.courseExamActions.forEach(element => {
        element.agree = false;
      });
    }
    if (this.data.agree === true) {
      this.data.reject = false;
    }
    this.onSelectionChange();
  }

  onRejectChange(event: any) {
    if (this.data.reject === true) {
      this.data.courseExamActions.forEach(element => {
        element.agree = false;
      });
      this.data.agree = false;
    }
    this.onSelectionChange();
  }

  onSelectionChange(event?: any) {
    this.data.selected = this.data.courseExamActions.filter((x: any) => {
      return x.agree === true;
    }).length;
    this.selectionBadge = this._translateService.instant('ExamPeriodParticipateActions.SelectedBadge', this.data);
  }

  selectAll() {
    // agree
    this.data.agree = true;
    // set reject to false
    this.data.reject = false;
    // select all
    this.data.courseExamActions.forEach(element => {
      element.agree = true;
    });
    // do selection change
    this.onSelectionChange();
  }

}
