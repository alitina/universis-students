import { Component, OnInit } from '@angular/core';
import {ModalService} from '@universis/common';
import {Router} from '@angular/router';


@Component({
  selector: 'app-grades-error',
  templateUrl: './grades-error.component.html'
})
export class GradesErrorComponent implements OnInit {
  public isLoading?: boolean;
  public modalTitle?: string;
  public modalBody?: string;
  public courses?: Array<any>;

  constructor(private modalService: ModalService,
              private router: Router) {
  }

  ngOnInit() {
  }

  cancel() {
    if (this.isLoading) {
      return;
    }
    // close
    if (this.modalService.modalRef) {
      return  this.modalService.modalRef.hide();
    }
  }

  async ok() {
    if (this.modalService.modalRef) {
      return  this.modalService.modalRef.hide();
    }
  }
}
