import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../services/grades.service';
import {LoadingService} from '@universis/common';

@Component({
  selector: 'app-grades-internship',
  templateUrl: './grades-internship.component.html',
  styleUrls: ['./grades-internship.component.scss']
})
export class GradesInternshipComponent implements OnInit {

  public internshipInfo: any;
  public isLoading = true;

  constructor(private gradesService: GradesService,
              private loadingService: LoadingService) {
    this.loadingService.showLoading();
    this.gradesService.getInternshipInfo().then((res) => {
      this.internshipInfo = res;
      this.loadingService.hideLoading();
      this.isLoading = false;
    }).catch( err => {
      this.loadingService.hideLoading();
      this.isLoading = false;
      console.log(err);
    });
  }

  ngOnInit() {
  }

}
