import { Component, OnInit, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { GradesService } from '../../services/grades.service';
// import {ChartsModule} from 'ng2-charts';
import { TranslateService } from '@ngx-translate/core';
import { ChartConfiguration } from 'chart.js';

@Component({
  selector: 'app-grades-statbox',
  templateUrl: './grades-statbox.component.html',
  styleUrls: ['./grades-statbox.component.scss']
})
export class GradesStatboxComponent implements OnInit {

  @Input() title?: string;
  @Input() statsPeriod?: string;
  @Input() labelTotal?: string;
  @Input() labelPassed?: string;
  @Input() registeredCourses?: number | any;
  @Input() passedCourses?: number | any;
  @Input() passedGradeWeightedAverage?: string;
  @Input() passedGradeSimpleAverage?: string;
  @Input() totalEcts?: number;
  @Input() isTranscript?: boolean;
  @Input() registeredEcts?: number;
  public doughnutChartData?: number[];
  public doughnutChartType = 'doughnut';
  public doughnutChartLabels: string[] = [];
  public failedCourses?: number;
  public doughnutColors = [
    {
      backgroundColor: [
        '#2500dd',
        '#BDB2F5',
      ]
    }
  ];
  public doughnutChartOptions = {};
  constructor(private _contextService: AngularDataContext,
    private _gradeService: GradesService,
    private _translateService: TranslateService) {
  }

  public doughnutChartDatasets: ChartConfiguration<'doughnut'>['data']['datasets'] = [];

  ngOnInit() {
    // this.labelTotal = this.labelTotal ? this.labelTotal : this._translateService.instant('Grades.Total');
    this.labelTotal =  this._translateService.instant('Grades.Statbox.RegisteredCourses');

    this.labelPassed = this.labelPassed ? this.labelPassed : this._translateService.instant('Grades.Statbox.PassedCourses');
    // this calculation should be correct as getGradesWeightedAverage filters complex courses which count as one.
    // Remove this comment before committing to your MR.
    this.failedCourses = this.registeredCourses - this.passedCourses;
    this.doughnutChartData = [this.passedCourses, this.failedCourses];

    this.doughnutChartDatasets = [{ data: this.doughnutChartData, label: "Μαθήματα", backgroundColor: ['#2500dd','#BDB2F5',] }];


    const text = this.passedCourses + '/' + this.registeredCourses;
    this.doughnutChartOptions = {
      // cutoutPercentage: 80, replaced by cutout at chart.js 3.4.0 pie's cutout is 0
      cutout: "85%",
      legend: {
        display: false
      },
      centerText: {
        display: true,
        text: text
      },
      tooltips: {
        position: 'nearest'
      }
    };
    this._translateService.get('Grades.PassedCourses').subscribe(x => {
      this.doughnutChartLabels.push(x);
    });
    this._translateService.get('Grades.FailedCourses').subscribe(x => {
      this.doughnutChartLabels.push(x);
    });

  }

}
