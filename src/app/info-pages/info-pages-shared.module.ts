import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';

import * as el from "./i18n/info.el.json"
import * as en from "./i18n/info.en.json"

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        MostModule
    ],
    declarations: [

    ],
    exports: [

    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InfoPagesSharedModule {

    constructor(private _translateService: TranslateService) {
        this._translateService.setTranslation("el", el, true);
        this._translateService.setTranslation("en", en, true);
    }
}
