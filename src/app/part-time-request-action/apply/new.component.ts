import { Component, OnInit, OnDestroy, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {
  LoadingService,
  ErrorService, DIALOG_BUTTONS, ModalService, ToastService
} from '@universis/common';
import { Subscription, combineLatest } from 'rxjs';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { AdvancedFormComponent } from '@universis/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseError } from '@themost/client';
import { TranslateService } from '@ngx-translate/core';
import { EmptyValuePostProcessor } from '@universis/forms';
import {ProfileService} from "../../profile/services/profile.service";

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewComponent implements OnInit, OnDestroy {
  public isValid: any;
  public canApply: boolean = false;
  public message: string | undefined;

  constructor(private _context: AngularDataContext,
              private _loading: LoadingService,
              private _errorService: ErrorService,
              private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _router: Router,
              private _profileService: ProfileService,
              private _modalService: ModalService,
              private _toastService: ToastService

  ) {
  }


  get activeTab(): any {
    if (this.tabSet) {
      return this.tabSet.tabs.find((tab) => {
        return tab.active;
      });
    }
  }

  queryParamSubscription: Subscription | undefined;
  public isLoading = true;
  public formAction: string | undefined;

  @Input() model: any = {
    actionStatus: {
      alternateName: 'PotentialActionStatus'
    }
  };
  @ViewChild('tabSet', {
    static: true
  }) tabSet: TabsetComponent | undefined;
  @ViewChild('form1', {
    static: false
  }) form1: AdvancedFormComponent | undefined;

  async ngOnInit() {

    const student = await this._profileService.getStudent();
    this.queryParamSubscription = combineLatest(
      this._activatedRoute.queryParams,
      this._activatedRoute.params
    ).subscribe(async (results) => {
      const queryParams = results[0];
      const params = results[1];

      // validate code
      if (queryParams['code'] == null) {
        // new application
        this._loading.showLoading();

        Promise.all([
          this._context.model('StudyPrograms').where('id').equal(student.studyProgram.id)
            .select('info/studyType/alternateName as studyType', 'info/partTimeStudyProgram as partTimeStudyProgram').getItem(),
          this._context.model('RequestPartTimeProgramActions')
            .asQueryable()
            .and('academicYear').equal(student.department.currentYear.id || student.department.currentYear)
            .and('academicPeriod').equal(student.department.currentPeriod.id || student.department.currentPeriod)
            .expand('partTimeCategory')
            .orderByDescending('dateCreated')
            .getItem()
        ]).then((results1) => {
          // get current StudyProgram and check studyType - if student already attends part-time
          const studyProgram = results1[0];
          if (studyProgram && studyProgram.studyType === 'parttime') {
            this.canApply = false;
            this.message = 'PartTimeRequestActions.AlreadyAttends'
          } else {
            // check if student has already applied
            this.model = results1[1];
            if (this.model) {
              if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
                // load existing request
                return this._router.navigate(['/requests', 'PartTimeRequests', 'apply'], {queryParams: {code: this.model.code}})
              }
              if (this.model.actionStatus.alternateName !== 'PotentialActionStatus') {
                // load existing request
                return this._router.navigate(['/requests', 'PartTimeRequests', this.model.code, 'preview'])
              }
            } else {

              // check if student's study program is associated with part-time program
              if (studyProgram && studyProgram.partTimeStudyProgram) {
                this.model = {
                  actionStatus: {
                    alternateName: 'PotentialActionStatus'
                  }
                };
                Object.assign(this.model,
                  {
                    academicYear: student.department.currentYear,
                    // tslint:disable-next-line:max-line-length
                    academicPeriod: student.department.currentPeriod,
                    student: student.id,
                    name: this._translateService.instant('PartTimeRequestActions.PartTimeRequest')
                  });
                this.canApply = true;
              } else {
                this.canApply = false;
                this.message = 'PartTimeRequestActions.MissingPartTimeProgram'
              }
            }
          }
          // set form action
          if (this.canApply) {
            this.formAction = params['action'] ? `RequestPartTimeProgramActions/${params['action']}` : `RequestPartTimeProgramActions/apply`;
          }
          // and hide loading
          this._loading.hideLoading();
          this.isLoading = false;
        }).catch((err) => {
          this.isLoading = false;
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });

      } else {
        this._loading.showLoading();
        Promise.all([
          this._context.model('RequestPartTimeProgramActions').where('code').equal(queryParams['code'])
            .expand('partTimeCategory')
            .getItem()
        ]).then((results1) => {
          const item = results1[0];
          if (item == null) {
            // tslint:disable-next-line:max-line-length
            return this._errorService.navigateToError(new ResponseError('The specified application cannot be found or is inaccessible', 404.5));
          }
          this.model = item;
          if (this.model && this.model.actionStatus && this.model.actionStatus.alternateName==='PotentialActionStatus') {
            this.isValid = true
            // set form
            this.canApply = true;
            this.formAction = params['action'] ? `RequestPartTimeProgramActions/${params['action']}` : `RequestPartTimeProgramActions/apply`;
          }
          else {
            this.canApply = false;
            return this._router.navigate(['/requests', 'PartTimeRequests', this.model.code, 'preview'])
          }
          this._loading.hideLoading();
          this.isLoading = false;
        }).catch((err) => {
          this.isLoading = false;
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      }
    });
  }

  onDataChange(event: any): void {
    if (this.form1 && this.form1.form) {
      // enable or disable button based on form status
      this.isValid = this.form1?.form?.formio?.checkValidity(null, false, null, true);
    }
  }

  async beforeNext(nextTab: TabDirective): Promise<boolean> {

    try {
      if (nextTab.id === 'request-information') {
        return true;
      }

        // save application before attach documents
        if (nextTab.id === 'attach-documents') {
          // save action
          if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
            this._loading.showLoading();
            const formio = this.form1?.form?.formio;
            const data = formio.data;
            formio.setPristine(false);
            // parse data with empty value processor
            new EmptyValuePostProcessor().parse(this.form1?.formConfig, data);
            // save application if something changed
            const result = await this._context.model('RequestPartTimeProgramActions').save(data)
            // update current action
            Object.assign(this.model, result);
            this._loading.hideLoading();
          }
        }
        return true;
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
      return false;
    } finally {
      this._loading.hideLoading();
    }
  }

  next(): void {
    // get active tab
    this.isValid = this.form1?.form?.formio?.checkValidity(null, false, null, true);
    if (this.isValid) {

      const findIndex = this.tabSet?.tabs?.findIndex((tab) => {
        return tab.active;
      });
      if (findIndex < this.tabSet?.tabs?.length) {
        // set active tab
        const tab = this.tabSet?.tabs[findIndex + 1];
        this.beforeNext(tab).then((result) => {
          if (result) {
            tab.disabled = false;
            tab.active = true;
          }
        });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.model && this.model.actionStatus && this.model.actionStatus.alternateName === 'PotentialActionStatus' && this.model.id)
    {
      const ConfirmMessageTitle = this._translateService.instant('Requests.RemoveRequest');
      const ConfirmMessage = this._translateService.instant('Requests.RemoveRequestMessage');
      this._modalService.showDialog(ConfirmMessageTitle, ConfirmMessage, DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._loading.showLoading();
          this._context.model('RequestPartTimeProgramActions').remove({
            id: this.model.id
          }).then(request => {
            this._toastService.show(
              this._translateService.instant('Requests.RemoveRequest'),
              this._translateService.instant('Requests.CompletedMessage'), true, 2000);
            return this._router.navigate(['/requests/list']);
          }).catch(err => {
            this._loading.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }

      });
    }
  }
}
