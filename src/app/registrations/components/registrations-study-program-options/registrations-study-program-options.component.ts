import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { AngularDataContext } from '@themost/angular';
import { ProfileService } from '../../../profile/services/profile.service';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService } from '@universis/common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrations-study-program-options',
  templateUrl: './registrations-study-program-options.component.html',
})
export class RegistrationsStudyProgramOptionsComponent implements OnInit {
  // template bindings
  // application data
  public hasSelected = false;
  private loading = false;
  public errorMessage;
  @Input('programGroups') programGroups: any;

  public collapsedStatus: any = {}; // object containing the collapsed status

  constructor(private _translateService: TranslateService,
    private currRegService: CurrentRegistrationService,
    private _context: AngularDataContext,
    public bsModalRef: BsModalRef,
    private _profileService: ProfileService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _toastService: ToastrService,
    private router: Router,
    private _errorService: ErrorService,
    private _currentRegistrationService: CurrentRegistrationService) { }


  async ngOnInit() {
    // initialize state and ui status variable regarding the forms
    if (this.programGroups != null) {
      this.programGroups.forEach((group) => {
        // sort alphabetically
        group.groups.sort((a, b) => a.name.localeCompare(b.name));
        this.collapsedStatus[group.id] = true;
      });
    }
  }

  /**
   *
   * Toggles the collapsed status of group
   *
   */
  toggleCollapse(groupId: number): void {
    if (groupId in this.collapsedStatus) {
      this.collapsedStatus[groupId] = !this.collapsedStatus[groupId];
    }
  }
  closeModal() {
    // clear group selections before closing modal
    if (this.programGroups) {
      this.programGroups.forEach(group => {
        group.selectedCount = 0;
        group.groups.forEach((x) => {
          x.selected = false;
        });
      });
    }
    this.bsModalRef.hide();
  }
  async submit() {
    // get confirm message for accept or reject
    const groups = [];
    let ConfirmMessage: string;
    const object = await this._profileService.getStudent();
    const ConfirmMessageTitle = this._translateService.instant('StudyProgramOptions.SelectGroups');
    this.programGroups.filter((x) => x.selectedCount).forEach((x) => {
      groups.push.apply(groups, x.groups.filter((y) => y.selected));
    });
    if (groups.length > 0) {
      ConfirmMessage = this._translateService.instant('StudyProgramOptions.ConfirmMessage', { 'selected': groups.length });
      // show dialog
      this._modalService.showDialog(ConfirmMessageTitle, ConfirmMessage, DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this.showLoading(true);
          const actions = {
            object,
            groups
          };
          this._context.model('students/me/programGroupActions')
            .save(actions).then((res) => {

              if (res && res.actionStatus && res.actionStatus.alternateName === 'CompletedActionStatus') {
                // get complete message
                const CompleteMessage = this._translateService.instant('StudyProgramOptions.CompleteMessage');
                // hide loading
                this.showLoading(false);
                // show toast message
                this._toastService.success(CompleteMessage, ConfirmMessageTitle,
                  { timeOut: 3000, positionClass: 'toast-top-center', progressBar: false });
                // navigation to registration
                this.closeModal();
                sessionStorage.removeItem('availableClasses');
                this._currentRegistrationService.removeGroupData();
                sessionStorage.removeItem('availableProgramGroups');
                this.router.navigate(['/registrations/courses/checkout']);
              } else {
                this.showLoading(false);
                // collapse groups
                this.programGroups.forEach((group) => {
                  this.collapsedStatus[group.id] = true;
                });
                // an error occurred, show error message
                this.errorMessage = res.description;
              }
            }).catch(err => {
              this.showLoading(false);
              this.closeModal();
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
        }
      });
    }
  }
  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  onChange(group: any, subGroup: any) {
    if (group && group.maxAllowedGroups <= 1) {
      if (subGroup.selected) {
        group.groups.forEach((x) => {
          if (x.id !== subGroup.id) {
            x.selected = false;
          }
        });
      }
    }
    // get number of selected subgroups
    group.selectedCount = group.groups.filter((x) => x.selected).length;
    this.hasSelected = this.programGroups.filter((x) => x.selectedCount).length;
  }
}


