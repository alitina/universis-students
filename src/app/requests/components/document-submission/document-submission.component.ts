import {Component, OnInit, Input, EventEmitter} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-document-submission-request',
  templateUrl: './document-submission.component.html'
})
export class DocumentSubmissionComponent implements OnInit {

  @Input() public requestType: any;
  @Input() public request: any;
  public documentConfiguration: any;
  public attachmentTypes: any;
  public studentRequest: any = {};
  public loading: boolean = false;
  private requestModel: any;
  public documentStateChange: EventEmitter<any> = new EventEmitter<any>();
  public editMode : boolean = true;

  constructor(private _context: AngularDataContext,
              protected _errorService: ErrorService,
              protected _activatedRoute: ActivatedRoute,
              protected _translateService: TranslateService,
              protected _router: Router,
              protected _loading: LoadingService,
              protected _http: HttpClient,
              protected _modalService: ModalService,
              protected _toastService: ToastService
  ) {
  }

  async ngOnInit() {
    this.loading = true;
    this.documentConfiguration = await this._context.model('StudentRequestConfigurations')
      .asQueryable()
      .and('additionalType').equal(this.requestType)
      .expand('attachmentTypes($expand=attachmentType)')
      .getItem();
    if (this.documentConfiguration) {
      this.attachmentTypes = this.documentConfiguration.attachmentTypes;
    }
    // get request
    this.requestModel = `${this.requestType}s`
    this.studentRequest = await this._context.model(this.requestModel)
      .where('code').equal(this.request)
      .expand('attachments($expand=attachmentType)')
      .getItem();
    if (this.studentRequest) {
      this.attachmentTypes = this.studentRequest.attachmentTypes;
      this.editMode =this.studentRequest && this.studentRequest.actionStatus && this.studentRequest.actionStatus.alternateName==='PotentialActionStatus';
    }
    this.onSelectAttachDocuments();
    this.loading = false;
  }

  onFileSelect(event: { addedFiles: any[]; }, attachmentType: any) {
    // get new file.
    if (this.editMode) {
      const addedFile = event.addedFiles[0];
      const formData: FormData = new FormData();
      formData.append('file', addedFile, addedFile.name);
      formData.append('file[attachmentType]', attachmentType.id);
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();

      const postUrl = this._context.getService().resolve(`${'students/me/requests'}/${this.studentRequest.id}/attachments/add`);
      this._loading.showLoading();
      return this._http.post(postUrl, formData, {
        headers: serviceHeaders
      }).subscribe((result) => {
        // reload attachments
        this._context.model(this.requestModel)
          .where('code').equal(this.request)
          .expand('attachments($expand=attachmentType)')
          .getItem()
          .then((request) => {
            // set model attachments
            this.studentRequest.attachments = request.attachments;
            this.onSelectAttachDocuments();
            // refresh attachment
            this._loading.hideLoading();
          }).catch((err) => {
          console.log(err);
          window.location.reload();
          this._loading.hideLoading();
        });
      }, (err) => {
        console.log(err);
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }
  }

  onFileRemove(attachment: any): any {
    if (this.editMode) {
      this._loading.showLoading();
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();
      const postUrl = this._context.getService().resolve(`${'students/me/requests'}/${this.studentRequest.id}/attachments/${attachment.id}/remove`);
      return this._http.post(postUrl, attachment, {
        headers: serviceHeaders
      }).subscribe(() => {
        // reload attachments
        this._context.model(this.requestModel)
          .where('code').equal(this.request)
          .expand('attachments($expand=attachmentType)')
          .getItem()
          .then((request) => {
            // set model attachments
            this.studentRequest.attachments = request.attachments;
            // refresh attachment
            // hide loading
            this.onSelectAttachDocuments();
            this._loading.hideLoading();
          }).catch((err) => {
          console.log(err);
          window.location.reload();
          this._loading.hideLoading();
        });
      }, (err) => {
        console.log(err);
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }
  }


  async submit() {
    // change request status to Active
    // validate documents
    if (this.editMode) {
      const ConfirmMessageTitle = this._translateService.instant('Requests.Commit');
      const ConfirmMessage = this._translateService.instant('Requests.CommitMessage');
      this._modalService.showDialog(ConfirmMessageTitle, ConfirmMessage, DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          this._loading.showLoading();
          this._context.model(this.requestModel).save({
            id: this.studentRequest.id,
            actionStatus: {
              "alternateName": "ActiveActionStatus"
            }
          }).then(request => {
            this._toastService.show(
              this._translateService.instant('Requests.CompletedTitle'),
              this._translateService.instant('Requests.CompletedMessage'), true, 2000);
            return this._router.navigate(['/requests/list']);
          }).catch(err => {
            this._loading.hideLoading();
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }

      });
    }
  }

  onSelectAttachDocuments(): void {

    this.attachmentTypes = this.attachmentTypes || [];
    this.attachmentTypes = this.studentRequest.attachmentTypes.map((item: any) => {
      const res = {
        attachmentType: item.attachmentType,
        numberOfAttachments: item.numberOfAttachments
      };
      // try to search model attachments list
      const findAttachment = this.studentRequest.attachments.find((attachment) => {
        if (attachment.attachmentType == null) {
          return false;
        }
        return attachment.attachmentType.id === item.attachmentType.id;
      });
      if (findAttachment) {
        // assign data
        Object.assign(res, {
          attachment: findAttachment
        });
      }
      return res;
    });
    this.documentStateChange.emit(this.documentState);
  }

  get documentState(): string {
    if (this.studentRequest.attachmentTypes == null) {
      return 'invalid';
    }
    const required = this.attachmentTypes.filter((item) => {
      return item.numberOfAttachments > 0 && item.attachment == null;
    }).length;
    if (required > 0) {
      return 'invalid';
    }
    return 'valid';
  }
}
