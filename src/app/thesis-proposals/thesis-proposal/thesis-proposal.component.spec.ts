import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesisProposalComponent } from './thesis-proposal.component';

describe('ThesisProposalComponent', () => {
  let component: ThesisProposalComponent;
  let fixture: ComponentFixture<ThesisProposalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThesisProposalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ThesisProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
