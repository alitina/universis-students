import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, TemplateRef, ViewChild } from '@angular/core';
import { ThesisProposalsService } from '../thesis-proposals.service';
import { Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, ErrorService, ToastService } from '@universis/common';
import * as Quill from 'quill';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import { ResponseError } from '@themost/client';

@Component({
  selector: "app-thesis-proposal",
  templateUrl: "./thesis-proposal.component.html",
  styles: [
    `
      .ql-toolbar.ql-snow {
        border: 1px solid #e4e7ea;
      }
      .ql-toolbar {
        border-top-left-radius: 0.375rem !important;
        border-top-right-radius: 0.375rem !important;
      }
      .ql-container.ql-snow {
        border: 1px solid #e4e7ea;
      }
      .ql-container {
        font-family: inherit;
        line-height: 1.75;
        min-height: 192px;
        font-size: 0.875rem;
        border-bottom-left-radius: 0.375rem !important;
        border-bottom-right-radius: 0.375rem !important;
      }
      @media only screen and (max-width: 800px) {
        .mobile-hidden {
          display: none;
        }
      }

      @media only screen and (min-width: 800px) {
        .mobile-show {
          display: none;
        }
      }
    `,
  ]
})

export class ThesisProposalComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  @Output() updatedItem = new EventEmitter<number>();
  @Input() thesisProposal: any;
  @ViewChild('reason') reason!: ElementRef;
  private reasonEditor: any;
  private QuillEditor: any = Quill;
  public notes: string = '';
  public isLoading: boolean = true;
  public expressInterest!: boolean;
  public modalRef!: BsModalRef;

  constructor(
    private _elementRef: ElementRef,
    private _router: Router,
    private _context: AngularDataContext,
    private _loading: LoadingService,
    private _modalService: BsModalService,
    private _errorService: ErrorService,
    private _thesesService: ThesisProposalsService,
    private _translateService: TranslateService,
    private _toastService: ToastService) { }

  ngAfterViewInit() {
    const QuillEditor: any = Quill;
    this.reasonEditor = new QuillEditor(this.reason.nativeElement, {
      theme: "snow",
    });
    if (this.notes) {
      this.reasonEditor.root.innerHTML = this.notes;
    }
    this.reasonEditor.on("text-change", (event: any) => {
      this.notes = this.reasonEditor.root.innerHTML;
      this.modelChange.emit(this.notes);
    });
  }

  async ngOnChanges(changes: SimpleChanges) {
    this.isLoading = true;
    this._loading.showLoading();
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        let change = changes[propName];
        switch (propName) {
          case 'thesisProposal': {
            this._thesesService.updateShowStatus(false);
            this.thesisProposal = change.currentValue;
            Object.assign(this.thesisProposal, { show: false });
            this.expressInterest = this.thesisProposal.STUDENT_INTERESTED;
            if (this.thesisProposal.STUDENT_INTERESTED || this.thesisProposal.STUDENT_ACCEPTED) {
              const interestNotes = await this._thesesService.getSingleThesisRequest(this.thesisProposal.id);
              Object.assign(this.thesisProposal, { interestNotes: interestNotes.notes });
              console.log("🚀 ~ ThesisProposalComponent ~ ngOnChanges ~ this.thesisProposal.:", this.thesisProposal)

            }
            this.isLoading = false;
            this._loading.hideLoading();
            return;
          }
        }
      }
    }
  }

  async ngOnInit() {
    //
  }

  async submit() {
    try {
      this._loading.showLoading();
      this.isLoading = true;
      await this._context.model(`students/me/thesisProposal/${this.thesisProposal.id}/request`).save({ notes: this.notes });
      const res = await this._thesesService.getSingleThesisProposal(this.thesisProposal.id);
      this._toastService.clear();
      this._toastService.show(
        this._translateService.instant('ThesisProposals.ModalMessage.SuccessTitle'),
        this._translateService.instant('ThesisProposals.ModalMessage.SuccessMessage')
      );
      this._loading.hideLoading();
      this.isLoading = false;
      this.updatedItem.emit(this.thesisProposal.id);
      this._router.navigate(['/thesis-proposals/browse']);
    } catch (err) {
      this._toastService.clear();
      this._toastService.show(
        this._translateService.instant('ThesisProposals.ModalMessage.FailTitle'),
        this._translateService.instant('ThesisProposals.ModalMessage.FailMessage')
      );
      this._loading.hideLoading();
      this.isLoading = false;
    }
  }

  toggleForm() {
    this.expressInterest = !this.expressInterest;
    if (this.expressInterest === true) {
      setTimeout(() => this.scrollToAnchor("student-form"), 100);
    }
  }

  scrollToAnchor(anchorId: string) {
    const element = this._elementRef.nativeElement.querySelector(`#${anchorId}`);
    if (element) {
      element.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest",
      });
    }
  }

  ngOnDestroy(): void {
    this._thesesService.updateShowStatus(true);
  }

  toggleMore() {
    this.thesisProposal.show = !this.thesisProposal.show;
  }

  acceptThesisModal(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }

  async acceptThesis() {
    this._loading.showLoading();
    this.modalRef.hide();
    try {
      const studentAction = this.thesisProposal.actions.find((el) => el.actionStatus && el.actionStatus.alternateName === "ActiveActionStatus" && el.owner === this.thesisProposal.student);
      if (studentAction) {
        studentAction.actionStatus = {
          alternateName: "CompletedActionStatus",
        };
        return await this._context
          .model(`students/me/thesisRequests/${studentAction.initiator}/actions/${studentAction.id}`).save(studentAction)
          .then(async () => {
            this._toastService.show(this._translateService.instant('ThesisProposals.ModalMessage.SuccessTitle'),
            this._translateService.instant('ThesisProposals.ModalMessage.SuccessMessage'));
            this._loading.hideLoading();
            this.isLoading = false;
            this.updatedItem.emit(this.thesisProposal.id);
            this._router.navigate(['/thesis-proposals/browse']);
          });
      } else {
        this.isLoading = false;
        this._loading.hideLoading();
        return this._errorService.showError(new ResponseError('', 404), {
          continueLink: '/thesis-proposals/browse'
        });
      }
    } catch (err: | any) {
      this.isLoading = false;
      this._loading.hideLoading();
      return this._errorService.showError(new ResponseError('', 404), {
        continueLink: '/thesis-proposals/browse'
      });
    }
  }

  returnToList() {
    this._thesesService.updateShowStatus(true);
    this._router.navigate(['/thesis-proposals/browse']);
  }

  rejectThesisModal(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }

  async rejectThesis() {
    this._loading.showLoading();
    this.modalRef.hide();
    try {
      const studentAction = this.thesisProposal.actions.find((el) => el.actionStatus && el.actionStatus.alternateName === "ActiveActionStatus" || (el.actionStatus.alternateName === "CompletedActionStatus" && el.owner === this.thesisProposal.student));
      if (studentAction) {
        studentAction.actionStatus = {
          alternateName: "CancelledActionStatus",
        };
        return await this._context
          .model(`students/me/thesisRequests/${studentAction.initiator}/actions/${studentAction.id}`).save(studentAction)
          .then(async () => {
            this._toastService.show(this._translateService.instant('ThesisProposals.ModalMessage.SuccessTitle'),
              this._translateService.instant('ThesisProposals.ModalMessage.SuccessMessage')
            );
            this._loading.hideLoading();
            this.isLoading = false;
            this.updatedItem.emit(this.thesisProposal.id);
            this._router.navigate(['/thesis-proposals/browse']);
          }
          );
      } else {
        this.isLoading = false;
        this._loading.hideLoading();
        return this._errorService.showError(new ResponseError('', 404), {
          continueLink: '/thesis-proposals/browse'
        });
      }
    } catch (err: | any) {
      this.isLoading = false;
      this._loading.hideLoading();
      return this._errorService.showError(new ResponseError('', 404), {
        continueLink: '/thesis-proposals/browse'
      });
    }
  }

  onFileSelect(event: any): any {
    //   // get file
    //   const addedFile = event.addedFiles[0];
    //   const formData: FormData = new FormData();
    //   formData.append('file', addedFile, addedFile.name);
    //   formData.append('attachmentType', attachmentType.id);
    //   formData.append('published', 'true');
    //   // get context service headers
    //   const serviceHeaders = this._context.getService().getHeaders();
    //   const postUrl = this._context.getService().resolve(`${'DiningRequestActions'}/${this.model.id}/addAttachment`);
    //   this._loading.showLoading();
    //   return this._http.post(postUrl, formData, {
    //     headers: serviceHeaders
    //   }).subscribe((result) => {
    //     // reload attachments
    //     this._context.model(`${'DiningRequestActions'}/${this.model.id}/attachments`)
    //       .asQueryable().expand('attachmentType')
    //       .getItems()
    //       .then((attachments) => {
    //         // set model attachments
    //         this.model.attachments = attachments;
    //         // refresh attachment
    //         // hide loading
    //         this._loading.hideLoading();
    //       }).catch((err) => {
    //         console.log(err);
    //         window.location.reload();
    //         this._loading.hideLoading();
    //       });
    //   }, (err) => {
    //     console.log(err);
    //     this._loading.hideLoading();
    //     this._errorService.showError(err, {
    //       continueLink: '.'
    //     });
    //   });
  }

  onFileRemove(attachment: any): any {
    //   this._loading.showLoading();
    //   // get context service headers
    //   const serviceHeaders = this._context.getService().getHeaders();
    //   const postUrl = this._context.getService().resolve(`${'DiningRequestActions'}/${this.model.id}/removeAttachment`);
    //   return this._http.post(postUrl, attachment, {
    //     headers: serviceHeaders
    //   }).subscribe(() => {
    //     // reload attachments
    //     this._context.model(`${'DiningRequestActions'}/${this.model.id}/attachments`)
    //       .asQueryable().expand('attachmentType')
    //       .getItems()
    //       .then((attachments) => {
    //         // set model attachments
    //         this.model.attachments = attachments;
    //         // refresh attachment
    //         // hide loading
    //         this._loading.hideLoading();
    //       }).catch((err) => {
    //         console.log(err);
    //         window.location.reload();
    //         this._loading.hideLoading();
    //       });
    //   }, (err) => {
    //     console.log(err);
    //     this._loading.hideLoading();
    //     this._errorService.showError(err, {
    //       continueLink: '.'
    //     });
    //   });
  }
}
