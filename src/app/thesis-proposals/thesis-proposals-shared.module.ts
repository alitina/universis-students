import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ProfileSharedModule } from '../profile/profile-shared.module';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import * as el from "./i18n/theses.el.json"
import * as en from "./i18n/theses.en.json"
import { ThesisProposalsService } from './thesis-proposals.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MostModule,
    TranslateModule,
    ProfileSharedModule,
    TooltipModule
  ]
})
export class ThesisProposalsSharedModule { 
  constructor(private _translateService: TranslateService) {
    this._translateService.setTranslation("el", el, true);
    this._translateService.setTranslation("en", en, true);
  }
  static forRoot(): ModuleWithProviders<ThesisProposalsSharedModule> {
    return {
      ngModule: ThesisProposalsSharedModule,
      providers: [
        ThesisProposalsService
      ]
    };
  }
}
