import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThesisProposalsService {

  private showList = new BehaviorSubject(true);
  toggleShow = this.showList.asObservable();

  private thesisProposalData = new BehaviorSubject({});
  dataChange = this.thesisProposalData.asObservable();

  constructor(private _context: AngularDataContext) {

  }

  getThesisProposals(academicYear: number): any {
    return this._context.model('students/Me/thesisProposals')
      .asQueryable()
      .where('startYear')
      .greaterOrEqual(academicYear)
      .expand('department,studyLevel,status,instructor($select=InstructorSummary),actions($expand=actionStatus),type($expand=locale)')
      .orderBy('dateCreated desc')
      .take(-1)
      .getItems();
  }

  getThesisProposalsSelectValues(): any {
    return this._context.model('students/Me/thesisProposals')
      .asQueryable()
      .where('status/alternateName').equal('potential')
      .expand('type($expand=locale),status')
      .select('type')
      .take(-1)
      .getItems();
  }

  searchThesisProposals(searchText: string = '', academicYear: number) {
    return this._context.model('students/Me/thesisProposals')
      .asQueryable()
      .and('name').contains(searchText)
      .where('startYear')
      .greaterOrEqual(academicYear)
      .expand('department,studyLevel,status,instructor($select=InstructorSummary),actions($expand=actionStatus),type($expand=locale)')
      .orderBy('dateCreated desc')
      .take(-1)
      .prepare();
  }

  getSingleThesisProposal(thesis: string): any {
    return this._context.model('students/Me/thesisProposals')
      .asQueryable()
      .where('id')
      .equal(thesis)
      .expand('status,type($expand=locale),department,studyLevel,instructor($select=InstructorSummary),actions($expand=actionStatus)')
      .getItem();
  }

  getThesisRequestsActions(): any {
    return this._context.model('thesisRequestActions')
      .asQueryable()
      .expand('actionStatus')
      .getItems();
  }

  getSingleThesisRequest(thesis: string): any {
    return this._context.model('thesisRequestActions')
      .asQueryable()
      .where('thesisProposal')
      .equal(thesis)
      .expand('actionStatus')
      .select('notes')
      .getItem();
  }

  updateShowStatus(show: boolean) {
    return this.showList.next(show);
  }


  async validateStudent(student: any) {
    let state: any  = {};
    state.valid = false;
    if (student.studentStatus && student.studentStatus.alternateName === 'active') {
      state.valid = true;
    }

    state.maximumAllowedThesis = (await this._context.model('students/me')
                                                     .asQueryable()
                                                     .where('id')
                                                     .equal(student.id)
                                                     .select('studyProgram/info/maximumAllowedThesis as maximumAllowedThesis')
                                                     .expand('studyProgram($expand=info)')
                                                     .getItem()).maximumAllowedThesis;
    if (state.maximumAllowedThesis === 0 || !state.maximumAllowedThesis) {
      state.maximumAllowedThesis = null;
    }
    return state;

  }

}
