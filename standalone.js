/*eslint-env node*/
/*eslint no-var: "off"*/
var fs = require('fs');
var rfs = require('rotating-file-stream');
var path = require('path');
var express = require('express');
var http = require('http');
var morgan = require('morgan');
var parseArgs = require('minimist')

// initialize express application
// https://expressjs.com/en/api.html#express
var app = express();
console.log("Initializing server");

// setup logger directory
var argv = parseArgs(process.argv.slice(2));
var logDir;

if ('log' in argv) {
    // check for --log log/directory CLI argument
    logDir = argv['log'];
} else if (('l' in argv) && (typeof argv['l'] != 'boolean')) {
    // -l log/directory argument
    logDir = String(argv['l']);
} else {
    // check for STUDENTS_APP_LOG env variable
    if ('STUDENTS_APP_LOG' in process.env &&
        process.env.STUDENTS_APP_LOG &&
        typeof process.env.STUDENTS_APP_LOG != 'boolean') {
        logDir = String(process.env.STUDENTS_APP_LOG);
    } else {
        logDir = path.join(process.cwd(), 'log');
    }
}

// make sure it is an absolute path
if (!path.isAbsolute(logDir)) {
    logDir = path.resolve(logDir);
}

console.log('Setting up log directory at ' + logDir);

// ensure log directory exists
if (fs.existsSync(logDir)) {
    console.log('Log directory found');
} else {
    fs.mkdirSync(logDir, {recursive: true});
    console.log('Log directory was created: ' + logDir);
}
// create a rotating write stream
var accessLogStream = rfs('access.log', {
    interval: '1d', // rotate daily
    path: logDir
});

// setup the logger
// https://github.com/expressjs/morgan
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":x-forwarded-for"', { stream: accessLogStream }));
// setup morgan token remote-addr
morgan.token('x-forwarded-for', function (req) {
    return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
});

// handle config file location
var configFile;
var userConfig = false; // flag to see if the user gave any arguments

if ('config' in argv) {
    // check for --config filename.json CLI argument
    configFile = argv['config'];
    userConfig = true;
} else if (('c' in argv) && (typeof argv['c'] != 'boolean')) {
    // -c filename.json argument
    configFile = String(argv['c']);
    userConfig = true;
} else {
    // check for STUDENTS_APP_CONFIG env variable
    if ('STUDENTS_APP_CONFIG' in process.env &&
        process.env.STUDENTS_APP_CONFIG &&
        typeof process.env.STUDENTS_APP_CONFIG != 'boolean') {
        configFile = String(process.env.STUDENTS_APP_CONFIG);
        userConfig = true;
    } else {
        // fall back to default app.production.json on cwd
        userConfig = false;
        configFile = path.join(process.cwd(), 'app.production.json');
        console.warn('The --config cli argument was not passed and env ' +
                     'STUDENTS_APP_CONFIG was not set, checking current ' +
                     'working directory for the default file \(' +
                     path.join(process.cwd(), 'app.production.json') + '\)');
    }
}

// make sure it is an absolute path
if (!path.isAbsolute(configFile)) {
    configFile = path.resolve(configFile);
}

console.log('Searching for config file: ' + configFile);
if (fs.existsSync(configFile)) {
    // the file (user input or default) exists, proceed
    console.log('Config file found');
} else {
    if (userConfig) {
        // if the user passed a parameter and the file is not found, crash
        console.error('Config file was not found at ' + configFile + '\n' +
                      'Check the parameter for typos and rerun the program. ' +
                      'To create a config file with default values, run the ' +
                      'program without specifying a config location.');
        process.exit(1);
    } else {
        // no parameter was passed, create app.production.json at cwd
        try {
            fs.copyFileSync(path.join(__dirname, 'dist/assets/config/app.json'),
                            configFile);
            console.log('Config file was created: ' + configFile);
        } catch (error) {
            console.error('Default config file could not be created.');
            console.error(error);
            process.exit(1);
        }
    }
}

// serve static files
// https://expressjs.com/en/starter/static-files.html
// Serve app.production.json from the calculated directory and disable cache
app.use('/assets/config/app.production.json', express.static(configFile, {
    lastModified: false,
    cacheControl: false,
    etag: false
}));

// serve all other static files from inside the binary
app.use(express.static(path.join(__dirname, 'dist')));

// get port from environment and store in Express.
var port = normalizePort(process.env.PORT || '7001');
app.set('port', port);

// get ip from environment and store in Express.
var address = process.env.IP || '127.0.0.1';
app.set('address', address);

// create http server
var server = http.createServer(app);

// Listen on provided port and address
server.listen(port, address, function() {
    if (process.send) {
        process.send('online');
    } else {
        //
    }
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    try {
        const addr = server.address();
        console.log('Listening on http://' + addr.address + ':' + addr.port)
    }
    catch(err) {
        // do nothing
    }
}

